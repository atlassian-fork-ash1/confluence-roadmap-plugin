AJS.toInit(function () {
    // @@roadmap
    var roadmapPopupTemplate, $roadmapPopup, $roadmapEditorPopup, roadmapEditorView, isCreateNew;
    var PAGE_LINK_DELIMITER = "~~~~~";

    AJS.MacroBrowser.setMacroJsOverride('roadmap', { opener:function (macro) {
        var updatePageLinksFromParams = function(roadmap, pagelinks, maplinks, callback) {
                var mapLinkArray = maplinks ? maplinks.split(PAGE_LINK_DELIMITER) : [];
                var pageLinkArray = pagelinks ? pagelinks.split(PAGE_LINK_DELIMITER) : [];

                if (!_.isEmpty(mapLinkArray)) {
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json",
                        url: AJS.contextPath() + '/rest/roadmap/1.0/extractPageLinks',
                        data: JSON.stringify({
                            'wikiLinks': _.object(mapLinkArray, pageLinkArray),
                            'roadmapSpace': AJS.Meta.get('space-key')
                        }),
                        success: function(result) {
                            _.each(roadmap.lanes, function(lane) {
                                _.chain(lane.bars)
                                    .filter(function(bar) {
                                        return _.contains(mapLinkArray, bar.id);
                                    })
                                    .each(function(bar) {
                                        if (result[bar.id]) {
                                            bar.pageLink = result[bar.id];
                                        }
                                    });
                            })
                            callback && callback();
                        },
                        error: function(error) {
                            AJS.log(error);
                            callback && callback();
                        }
                    });
                } else {
                    callback && callback();
                }
            },

            preparePageLinkParams = function(roadmapData) {
                var pageLinks = [];
                var mapLinks = [];
                roadmapData.lanes.forEach(function(lane) {
                    lane.bars.forEach(function(bar) {
                        if(!_.isEmpty(bar.pageLink)) {
                            var wikiLink = bar.pageLink.wikiLink.replace(/[\][]/g,'');  //remove "[ ]"
                            pageLinks.push(_.escape(wikiLink));
                            mapLinks.push(bar.id);
                        }
                    });
                });
                return {
                    pageLinks: pageLinks.join(PAGE_LINK_DELIMITER),
                    mapLinks: mapLinks.join(PAGE_LINK_DELIMITER)
                }
            };

        // Prevent user from editing an old roadmap
        // When user try to add a new roadmap, macro.params value is undefined
        if (macro.params && macro.params.timeline !== 'true') {
            var dialogId = 'roadmap-deprecation-dialog';
            var $dialogEl = $(Confluence.Templates.Roadmap.deprecationDialog({
                id: dialogId
            }));

            AJS.dialog2($dialogEl).on('hide', function() {
                $('#'+dialogId).remove();
            }).on('show', function() {
                $("#dialog-close-button", $dialogEl).click(function(e) {
                    e.preventDefault();
                    AJS.dialog2('#'+dialogId).hide();
                });
            }).show();

            return false; // Return to exit this function.
        }

        // Setting up to show the Roadmap Editor Dialog
        var roadmapData;
        var constructRoadmapDialog = function() {

            var roadmapDialogId = 'confluence-roadmap-dialog';
            var roadmapPopupPanelId = 'roadmap-popup-panel';
            var roadmapDialog;

            if (typeof roadmapPopupTemplate == "undefined") {
                roadmapPopupTemplate = aui.dialog.dialog2({
                    id: roadmapDialogId,
                    size: "xlarge",
                    titleText: AJS.I18n.getText('roadmap.editor.dialog.title'),
                    content: Confluence.Templates.Roadmap.roadmapPopupPanel({roadmapPopupPanelId: roadmapPopupPanelId}),
                    footerActionContent: Confluence.Templates.Roadmap.footerActionContent(),
                    /* this option prevents dialog2 from closing when a dropdown2 is going to show */
                    modal: true
                });

                $roadmapPopup = $(roadmapPopupTemplate).appendTo($("body"));
                $roadmapPopup.find("#roadmapInsertBtn").on("click", function (e) {
                    roadmapEditorView.removeDialog();
                    var roadmapData = roadmapEditorView.model.toMacroData();
                    var source = encodeURIComponent(JSON.stringify(roadmapData));
                    var hash = hex_md5(source);

                    var pageLinkRoadmap = preparePageLinkParams(roadmapData);
                    var macroData = {
                        timeline: 'true',
                        source: source,
                        hash: hash,
                        title: encodeURIComponent(roadmapData.title),
                        pagelinks: pageLinkRoadmap.pageLinks,
                        maplinks: pageLinkRoadmap.mapLinks
                    };

                    tinymce.confluence.macrobrowser.macroBrowserComplete({name: 'roadmap', params: macroData});

                    roadmapEditorView.remove();
                    roadmapDialog.hide();
                    if (isCreateNew) {
                        Confluence.Roadmap.Analytics.createRoadmap(roadmapData);
                    } else {
                        Confluence.Roadmap.Analytics.editRoadmap(roadmapData);
                    }
                });
                $roadmapPopup.find("#roadmapCancelBtn").on("click", function (e) {
                    roadmapEditorView.removeDialog();
                    roadmapEditorView.remove();
                    roadmapDialog.hide();
                });

                $roadmapEditorPopup = $roadmapPopup.find('.roadmap-editor-popup');
            }

            roadmapDialog = AJS.dialog2("#" + roadmapDialogId).show();
            // Each time we call 'roadmapEditorView.remove()', it will remove roadmapPopupPanel.
            // To avoid this, we create a new container for roadmap-view.
            var $popupPanel = $('#' + roadmapPopupPanelId);
            var $roadmapViewEl = $popupPanel.find('.roadmap-view');

            if (!$roadmapViewEl.length) {
                $roadmapViewEl = $('<div class="roadmap-view"/>');
                $popupPanel.append($roadmapViewEl);
            }

            var roadmap = new Roadmap.Roadmap(roadmapData, {parse: true});
            roadmapEditorView = new Roadmap.RoadmapEditorView({
                model: roadmap,
                dialogId: roadmapDialogId,
                el: $roadmapViewEl,
                roadmapEditorPopup: $roadmapEditorPopup
            });
            roadmapEditorView.render();

            resizePopupPanel();
        };

        if (macro.params && macro.params.source) { // edit roadmap
            isCreateNew = false;
            roadmapData = JSON.parse(decodeURIComponent(macro.params.source));
            updatePageLinksFromParams(roadmapData, macro.params.pagelinks, macro.params.maplinks, constructRoadmapDialog);
        } else { // create roadmap from macro browser or macro suggestion
            isCreateNew = true;
            Confluence.Roadmap.Analytics.insertRoadmap(); // insert new roadmap into editor
            constructRoadmapDialog();
        }
    }});

    function resizePopupPanel() {
        setTimeout(function() {
            if ($roadmapPopup && $roadmapPopup.is(':visible')) {
                var $popupHeight = $roadmapPopup.height(),
                    $titleHeight = $roadmapPopup.find('.aui-dialog2-header').outerHeight(),
                    $buttonsHeight = $roadmapPopup.find('.aui-dialog2-footer').outerHeight(),
                    $toolbarHeight = $roadmapPopup.find('#roadmap-editor-toolbar').outerHeight(),
                    $roadmapEditorPopup = $roadmapPopup.find('.roadmap-editor-popup');

                $roadmapEditorPopup.css('height', $popupHeight - $titleHeight - $buttonsHeight - $toolbarHeight);
            }
        }, 0);
    }

    $(window).resize(function() {
        resizePopupPanel();
        roadmapEditorView && roadmapEditorView.resize();
    })
});
