(function($, _) {
    window.Roadmap = window.Roadmap || {};

    Roadmap.RoadmapEditorView = Backbone.View.extend({
        className: 'roadmap-editor-container',

        events: {
            'BarView.add': '_onAddBar',
            'BarView.dragstart': '_onBarDragstart',
            'BarView.dragstop': '_onBarDragstop',
            'BarView.drop': '_onBarDropped',
            'LaneView.drop': '_onBarDropped',
            'Lane.delete': '_onDeleteLane',
            'BarRowView.remove': '_onBarRowViewRemove',
            'sortstart .roadmap-content': '_onSortStart',
            'sortstop .roadmap-content': '_onSortStop',
            'remove': '_onRemove'
        },

        initialize: function() {
            _.bindAll(this, 'render');

            this.listenTo(this.model.get('lanes'), 'add', function(laneModel) {
                var laneView = new Roadmap.LaneView({model: laneModel, timelineView: this._timelineView, canDelete: true});
                if (this._laneViews.length === 1) {
                    this._laneViews[0].updateDeleteStatus(true); // when has more than 2 lanes, lanes can be delete
                }
                this._laneViews.push(laneView);

                this.$roadmapContent.append(laneView.render().$el);
                this._updateTimelineColumnHeight();
                this._scrollToBottom();
            });

            this.listenTo(this.model.get('markers'), 'add', function(markerModel) {
                var markerView = new Roadmap.MarkerView({model: markerModel, roadmap: this});
                this.$roadmapMarker.append(markerView.render().$el);
                this._markerViews.push(markerView);
            });

            this.listenTo(this.model.get('timeline'), 'change', function() {
                if (this._isTimelineChanged()) {
                    this._calculateMinWidthOfABar();
                    this._timelineView.update();
                    this._timelineColumnView.update();
                    this.$roadmapContent.empty();
                    _.each(this._laneViews, function(laneView) {
                        laneView.remove();
                    });
                    this._laneViews = [];
                    this._renderLanes();

                    this.$roadmapMarker.empty();
                    this._markerViews = [];
                    this._renderMarkers();
                    this._updateDragArea();
                }
            });
        },

        render: function() {
            this.$el.html(Confluence.Templates.Roadmap.roadmapEditor());

            this._calculateMinWidthOfABar();
            this._initVariables();
            this._renderToolbar();
            this._renderTimeline();
            this._renderLanes();
            this._renderMarkers();
            this._renderTimelineColumn();

            this._updateDragArea();

            return this;
        },

        _calculateMinWidthOfABar: function() {
            Roadmap.barMinWidth = this.model.get('timeline').get('displayOption') === Roadmap.TIMELINE_DISPLAY_OPTION.MONTH
                                    ? Roadmap.MONTH_BAR_MIN_WIDTH
                                    : Roadmap.WEEK_BAR_MIN_WIDTH;
        },

        _initVariables: function() {
            this.$roadmapContainer = this.$el.find('#roadmap-container');
            this.$roadmapTimeline = this.$roadmapContainer.find('.roadmap-frame');
            this.$roadmapContent = this.$roadmapContainer.find('.roadmap-content');
            this.$roadmapMarker = this.$roadmapContainer.find('.roadmap-marker');

            this._timelineView = new Roadmap.TimelineView({ model: this.model.get('timeline')});
            this._timelineColumnView = new Roadmap.TimelineColumnView({ model: this.model.get('timeline')});
            this._laneViews = [];
            this._markerViews = [];
        },

        _renderTimeline: function() {
            this.$roadmapTimeline.prepend(this._timelineView.render().$el);
        },

        _renderTimelineColumn: function() {
            this._updateTimelineColumnHeight();
            this.$roadmapTimeline.append(this._timelineColumnView.render().$el);
        },

        _renderMarkers: function() {
            var me = this;
            this.model.get('markers').each(function(markerModel) {
                var markerView = new Roadmap.MarkerView({model: markerModel, roadmap: me});
                me.$roadmapMarker.append(markerView.render().$el);
                me._markerViews.push(markerView);
            });
        },

        _renderLanes: function() {
            Confluence.Roadmap.ColorHelper.resetColorCounts();
            var me = this;
            this._updateLaneWidth();
            var canDelete = this.model.get('lanes').models.length > 1; // can not delete the last lane
            this.model.get('lanes').each(function(laneModel) {
                var laneView = new Roadmap.LaneView({model: laneModel, timelineView: me._timelineView, canDelete: canDelete});
                me._laneViews.push(laneView);
                me.$roadmapContent.append(laneView.render().$el);
            });

            // Setup Lanes Re-ordering
            this._setupLaneReordering();
        },

        _onAddBar: function(event, barView) {
            this._updateTimelineColumnHeight();
            var $barEl = barView.$el;
            if (!this._isInViewScreen($barEl)) {
                this._scrollToBar($barEl);
            }
        },

        _renderToolbar: function() {
            var $dialogBody = $('#' + this.options.dialogId + ' .aui-dialog2-content');
            $dialogBody.find('.roadmap-toolbar').remove();
            var toolbarView = new Roadmap.ToolbarView({
                model: this.model,
                timelineView: this._timelineView
            });
            $dialogBody.prepend(toolbarView.render().$el);
        },

        _getTimelineWidth: function() {
            return this.$roadmapTimeline.find('.roadmap-frame-title').width();
        },

        _getLaneHeight: function() {
            return this.$roadmapContent.height();
        },

        _updateLaneWidth: function() {
            this.$roadmapContent.width(this._getTimelineWidth() + Roadmap.LANE_TITLE_WIDTH);
        },

        _scrollToBottom: function() {
            this.options.roadmapEditorPopup.scrollTop(this._getLaneHeight());
        },

        _isInViewScreen: function($elem) {
            return $.inviewport($elem, {threshold : -200});
        },

        _scrollToBar: function($bar) {
            this.options.roadmapEditorPopup.scrollTop($bar.parent().position().top);
            this.options.roadmapEditorPopup.scrollLeft(0);
        },

        _onBarDropped: function() {
            this._updateTimelineColumnHeight();
        },

        _onDeleteLane: function(event, laneView) {
            this.model.get('lanes').remove(laneView.model);
            this._laneViews.splice(_.indexOf(this._laneViews, laneView), 1);
            laneView.remove();
            this._updateTimelineColumnHeight();
            if (this._laneViews.length === 1) {
                // update delete status for can not delete the last lane
                this._laneViews[0].updateDeleteStatus(false);
            }
        },

        _onBarRowViewRemove: function() {
            this._updateTimelineColumnHeight();
        },

        _updateTimelineColumnHeight: function() {
            var laneHeight = this._getLaneHeight();
            this._timelineColumnView.updateHeight(laneHeight);
            _.each(this._markerViews, function(markerView) {
                markerView.updateHeight(laneHeight);
            });
        },

        hideDialog: function() {
            $('#inline-dialog-roadmap-dialog, #inline-dialog-timeline-options').hide();
        },

        removeDialog: function() {
            $('#inline-dialog-roadmap-dialog, #inline-dialog-timeline-options').remove();
        },

        _onBarDragstart: function(e, ui) {
            var me = this;
            this.hideDialog();
            this._updateDragArea();

            this._barRowNewViewList = [];
            _.each(this._laneViews, function(laneView) {
                me._barRowNewViewList.push(laneView.addNewBarRow(ui.helper));
            });
        },

        _updateDragArea: function() {
            // Create a drag-area that draggable element will be rendered to.
            var $dragZone = this.$roadmapContainer.find('.roadmap-drag-area');
            $dragZone.css(this._timelineColumnView.getSizeAndPosition());
        },

        _onBarDragstop: function() {
            // Remove Roadmap.BarRowNewView
            _.each(_.flatten(this._barRowNewViewList), function(barRowNew) {
                barRowNew.remove();

            });
            delete this._barRowNewViewList;

            _.each(this._laneViews, function(laneView) {
                laneView.removeEmptyRow();
            });
        },

        /* Lanes re-ordering */
        _setupLaneReordering: function() {
            this.$roadmapContent.sortable({
                axis: 'y',
                cursor: 'move',
                handle: '.roadmap-lane-title',
                opacity: 0.3,
                placeholder: 'roadmap-lane lane-sorting-placeholder',
                helper: function(e, el) {
                    return $(el).clone().addClass('lane-sorting-hepler');
                }
            });
        },

        _onSortStart: function(e, ui) {
            this.hideDialog();
            // Hide the vertical line, we will show it up again in _onSortStop function
            // Reason for this is decrease the effort to update height for them.
            this._timelineColumnView.$el.hide();

            // Format the placeholder
            ui.placeholder.css({
                height: Math.min(ui.helper.height(), 100),
                width: ui.helper.width()
            }).html(Confluence.Templates.Roadmap.lane(Roadmap.TRANSPARENT_LANE));

            // Set a specific width for roadmap-lane-content, because of limitation of display is table-cell.
            ui.helper.find('.roadmap-lane-content').css({
                width: ui.helper.width() - (ui.helper.find('.roadmap-lane-title').outerWidth() + ui.helper.find('.roadmap-separate-content').outerWidth())
            });
        },

        _onSortStop: function(e, ui) {
            // Re-show the timeline column which was hidden in _onSortStart
            this._timelineColumnView.$el.show();

            // lookup for lane which is reordered
            var laneViewId = ui.item.attr('cid');
            var laneView = _.find(this._laneViews, function(lane) {
                return lane.cid === laneViewId;
            });
            var laneModel = laneView.model;
            var laneCollection = this.model.get('lanes');

            // Remove from current collection
            laneCollection.remove(laneModel);

            // Insert model to new position
            laneCollection.models.splice(ui.item.index(), 0, laneModel);
            laneCollection._byId[laneModel.cid] = laneModel;
        },

        _onRemove: function() {
            this.$roadmapTimeline.remove();
        },
        /* End lanes re-ordering */

        _isMonthYearChanged: function() {
            var isDifferentMonthYear = function(date1, date2) {
                if (date1.getMonth() !== date2.getMonth() || date1.getYear() !== date2.getYear()) {
                    return true;
                }
                
                return false;
            };

            var timeline = this.model.get('timeline');
            return isDifferentMonthYear(timeline._previousAttributes.startDate, timeline.attributes.startDate) ||
                    isDifferentMonthYear(timeline._previousAttributes.endDate, timeline.attributes.endDate);
        },

        _isWeekChanged: function() {
            var isSameWeek = function(date1, date2) {
                var mDate1 = moment(date1);
                var mDate2 = moment(date2);

                return mDate1.get('year') === mDate2.get('year') && mDate1.isoWeek() === mDate2.isoWeek();
            };

            var timeline = this.model.get('timeline');
            var oldStartDate = timeline._previousAttributes.startDate;
            var currentStartDate = timeline.attributes.startDate;

            var oldEndDate = timeline._previousAttributes.endDate;
            var currentEndDate = timeline.attributes.endDate;

            return !(isSameWeek(oldStartDate, currentStartDate) && isSameWeek(oldEndDate, currentEndDate));
        },

        _isTimelineChanged: function() {
            var timeline = this.model.get('timeline');

            var isChanged = timeline.changed.displayOption;
            if (isChanged) {
                this.model.updateDurationUnit(timeline.get('displayOption'));
            } else {
                isChanged = timeline.get('displayOption') === Roadmap.TIMELINE_DISPLAY_OPTION.MONTH
                        ? this._isMonthYearChanged()
                        : this._isWeekChanged();
            }

            return isChanged;
        },

        resize: function() {
            this._updateLaneWidth();
            this._updateTimelineColumnHeight();
            this.$roadmapContent.css({
                top: this.$roadmapTimeline.find('.roadmap-column-title:first').innerHeight()
            });
        }
    });
})(AJS.$, window._);