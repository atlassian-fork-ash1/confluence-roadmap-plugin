/**
 * Currently InlineDialog doesn't support left/right orientation ( will support from version AUI 5.5.1 https://ecosystem.atlassian.net/browse/AUI-2197 )
 * SideInlineDialog has been already implemented and support left/right orientation
 * https://bitbucket.org/atlassianlabs/confluence-roadmap-plugin/src/f7c94dfb23007aa7fd0458af5e6128cdeacc6338/src/main/resources/js/SideInlineDialog.js?at=master
 * Temporary copied getDimensions & calculatePositions, getArrowPath functions to here with names_getSideDimensions & _calculateSidePositions, _getSideArrowPath
 * TODO update Roadmap InlineDialog use left/right option https://jira.atlassian.com/browse/CONFDEV-25613
 */
(function($, _) {
    Roadmap.BarView = Backbone.View.extend({
        className: 'roadmap-bar',

        events: {
            'click': '_showDialog',
            'dragstart': '_onDragstart',
            'dragstop': '_onDragStop',
            'dropover': '_onDropOver',
            'dropout': '_onDropOut'
        },

        attributes: function() {
            return {
                cid: this.cid
            };
        },

        initialize: function() {
            _.bindAll(this, 'render', '_updateBar', '_beforeResize', 'updateMaxWidth', '_deleteBar', '_updateTitle');

            this._addEvents();
        },

        _addEvents: function() {
            var me = this;

            this.$el.draggable({
                opacity: 0.3,
                cursor: "move",
                appendTo: '.roadmap-drag-area',
                snap: '.roadmap-bar-row, .new-bar-row',
                snapMode: 'inner',
                snapTolerance: 17,
                scroll: true,
                zIndex: 4020, // this will prevent the cursor change to resize when drag over two borders left/right
                helper: function() {
                    return me.$el.clone().addClass('roadmap-bar-clone').data('View', me);
                }
            });

            this.$el.droppable({
                accept: '.roadmap-bar',
                tolerance: 'touch',
                hoverClass: 'bar-hover-bar'
            });

            this.$el.data('View', this);

        },

        render: function() {
            this.updateColor(this.options.color);
            
            var barPosition = this.options.timelineView.getBarPosition(this.model.attributes);
            this.$el.css({
                left: barPosition.left + 'px',
                width: barPosition.width + 'px'
            });
            this.$el.attr('title', this.model.get('title'));

            this.$el.append(
                Confluence.Templates.Roadmap.bar({
                    title: this.model.get('title')
                })
            );

            this.$el.resizable({
                handles: 'e, w',
                minWidth: Roadmap.barMinWidth,
                start: this._beforeResize,
                stop: this._updateBar
            });
            return this;
        },

        updateMaxWidth: function(maxWidth) {
            this.$el.resizable('option', 'maxWidth', maxWidth);

            // min width is always smaller or equal max width
            var minWidth = this.$el.resizable('option', 'minWidth');
            if (maxWidth < minWidth) {
                this.$el.resizable('option', 'minWidth', maxWidth);
            }
        },

        _beforeResize: function(event, barUI) {
            this._barDialog && this._barDialog.remove(); // remove bar dialog before resize
            this.trigger('beforeResize', this, this._checkHandleDirection(event));
        },

        _checkHandleDirection: function(event) {
            var direction = $(event.toElement ? event.toElement : event.originalEvent.target);
            //we only support resize on left and right so don't need to check more
            if (direction.hasClass('ui-resizable-w')) {
                return 'left';
            } else if (direction.hasClass('ui-resizable-e')) {
                return 'right';
            }
            throw "Unsupport resize direction handling = " + direction;
        },

        _updateBar: function(event, barUI) {
            if (barUI.originalPosition.left != barUI.position.left) {
                this.model.set({
                    startDate: this.options.timelineView.getBarStartDate(barUI.position.left),
                    duration: this.options.timelineView.getBarDuration(barUI.element.outerWidth())
                });
            } else if (barUI.originalSize.width != barUI.size.width) {
                this.model.set('duration', this.options.timelineView.getBarDuration(barUI.element.outerWidth()));
            }
        },

        appendTo: function($container) {
            this.$el.appendTo($container);
        },

        updateColor: function(color) {
            this.$el.css({
                'background-color': color.bar,
                'border-color': color.lane,
                color: color.text
            });
        },

        updatePositionLeft: function(left) {
            this.$el.css('left', left);
        },

        _onDragStop: function(e, ui) {
            this.options.lane.cleanLaneAfterDrop();
            this.$el.trigger('BarView.dragstop');
        },

        /**
         * Update delete function from lane, called when bar is dropped into another lane.
         */
        updateDeleteBar: function(deleteBar) {
            this.options.deleteBar = deleteBar;
        },

        /**
         * Open the 'Bar' dialog.
         * Called when the 'bar' is clicked.
         */
        _showDialog: function(event) {
            if (!$(event.target).is('.ui-resizable-handle')) { // do not show dialog when resize
                this._barDialog && this._barDialog.remove(); // remove existing object if there is any
                this._barDialog = this._createDialog();
                this._barDialog.show();
                Confluence.Roadmap.Analytics.openBarDialogInEdit();
            }
        },

        _createDialog: function() {
            return new Roadmap.BarDialogView({
                trigger: this.$el,
                model: this.model,
                timelineWidth: this.options.timelineView.$el.width(),
                renderOption: {
                    isEditMode: true,
                    editInplace: {
                        title: true,
                        pageLink: true,
                        description: true
                    }
                },
                linkPageEditable: true, // user of course has edit page permission in edit mode
                deleteBar: this._deleteBar,
                updateTitle: this._updateTitle,
                updateDescription: this._updateDescription,
                updatePageLink: this._updatePageLink,
                createLinkPageCallback: this._goToCreateLinkPage
            });
        },

        _deleteBar: function() {
            this.options.deleteBar(this);
        },

        _updateTitle: function(newTitle) {
            this.model.set('title', newTitle);
            this.$el.attr('title', newTitle);
            this.$el.find('.roadmap-bar-title').html(AJS.escapeHtml(newTitle));
        },

        _updateDescription: function(newDescription) {
            this.model.set('description', newDescription);
        },

        _updatePageLink: function(pageLink) {
            this.model.set('pageLink', pageLink);
        },

        _onDragstart: function(e, ui) {
            Roadmap.DragDrop.barDraggingOver = {};
            this.$el.trigger('BarView.dragstart', [ui]);
        },

        _onDropOver: function(e, ui) {
            Roadmap.DragDrop.barDraggingOver[this.cid] = this.cid;
            if (Roadmap.DragDrop.barDraggingOver && ui.helper.data('isAddingNewRow') !== true) {
                ui.helper.addClass('roadmap-bar-overlapped');
            }
        },

        _onDropOut: function(e, ui) {
            var me = this;
            delete Roadmap.DragDrop.barDraggingOver[me.cid];

            ui.helper.removeClass('roadmap-bar-overlapped');

            _.each(Roadmap.DragDrop.barDraggingOver, function(item) {
                var isNotAddingNewRow = ui.helper.data('isAddingNewRow') !== true;

                if (isNotAddingNewRow) {
                    if (typeof item === 'string') { // still over on a bar
                        ui.helper.addClass('roadmap-bar-overlapped');
                    } else if (item.className === 'roadmap-bar-row') { // still in a bar-row
                        item.$el.addClass('roadmap-bar-drag-hover');
                    }
                }
            });
        },

        _goToCreateLinkPage: function(event) {
            event.preventDefault();
            var me = this.me;
            var currentPageId = parseInt(AJS.Meta.get('page-id'));
            var pageId = currentPageId > 0 ? currentPageId : AJS.Meta.get('parent-page-id');
            var linkParam = {
                roadmapBarId: me.model.id,
                roadmapContentId: pageId,
                updateRoadmap: false
            }
            Confluence.RoadmapLink.addCreateLinkPageListener(linkParam, function(barDetail) {
                var $pageLink = me._updatePageLink(barDetail.pageLink);
                me.fields.$pageLink.html($pageLink);
            });
        }
    });
})(AJS.$, window._);