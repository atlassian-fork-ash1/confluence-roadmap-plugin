(function($, _) {
    Roadmap.BarRowNewView = Backbone.View.extend({
        className: 'new-bar-row',

        overlayTop: 3, // number of pixel that will overlay on bar
        margin: 5, // margin between two bar-rows

        events: {
            drop: '_onDrop',
            dropover: '_onDropOver',
            dropout: '_onDropOut'
        },

        initialize: function() {
            _.bindAll(this, 'render', '_addEvent');

            return this;
        },

        render: function() {
            this.$el.append(Confluence.Templates.Roadmap.newRowLine());
            this._addEvent();
            this._setSizePosition();

            return this;
        },

        _addEvent: function() {
            this.$el.droppable({
                accept: '.roadmap-bar',
                tolerance: 'pointer',
                hoverClass: 'new-bar-row-hover'
            });
        },

        _setSizePosition: function() {
            var $barRow = this.options.$barRow;
            var barRowHeight = this.options.renderBottom === false ? (this.margin * -1) : $barRow.height();
            var isFirstRow = $barRow.index() === 0;
            var overlay = (isFirstRow) ? this.overlayTop + this.margin : this.overlayTop;

            this.$el.css({
                top: $barRow.position().top + barRowHeight - overlay,
                width: $barRow.width()
            });
        },

        _onDrop: function(e, ui) {
            this.$el.trigger('BarView.drop', [{
                barRowNewView: this
            }, ui]);
        },

        _onDropOver: function(e, ui) {
            this.$el.trigger('BarRowNew.dropover', [this, ui]);
        },

        _onDropOut: function(e, ui) {
            this.$el.trigger('BarRowNew.dropout', [this, ui]);
        }
    });
})(AJS.$, window._);