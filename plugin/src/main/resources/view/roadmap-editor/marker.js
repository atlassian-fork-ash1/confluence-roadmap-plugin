(function($, _) {
    window.Roadmap = window.Roadmap || {};

    Roadmap.MarkerView = Backbone.View.extend({
        className: 'vertical-line',

        events: {
            dragstop: '_onDragStop',
            dragstart: '_onDragStart'
        },

        initialize: function() {
            _.bindAll(this, 'render', 'updateHeight', 'renameTitle', '_renderComplete', '_onMarkerClick');
        },

        render: function() {
            var timeline = this.options.roadmap._timelineView.model;
            var markerPosition;
            if (timeline.get('displayOption') === Roadmap.TIMELINE_DISPLAY_OPTION.MONTH) {
                markerPosition = Confluence.Roadmap.Helper.getPosXOnMonthTimeline(timeline.attributes, this.model.get('markerDate'));
            } else {
                markerPosition = Confluence.Roadmap.Helper.getPosXOnWeekTimeline(timeline.attributes, this.model.get('markerDate'));
            }

            if (markerPosition < 0 || markerPosition > this.options.roadmap._timelineView.$el.width()) {
                return false;
            }
            this.$el.css({
                left: (markerPosition + Roadmap.LANE_TITLE_WIDTH) + 'px'
            });
            this.$el.append(
                Confluence.Templates.Roadmap.marker({title: this.model.get('title')})
            );
            this.updateHeight(this.options.roadmap._getLaneHeight());
            _.defer(this._renderComplete);
            return this;
        },

        _renderComplete: function() {
            this._setupMarkerDragging();
            this.$el.click(this._onMarkerClick);
            this.$el.find('.marker-title').ellipsis({row: Roadmap.MARKER_TITLE_LINE});
        },

        _setupMarkerDragging: function() {
            this.$el.draggable({
                axis: 'x',
                zIndex: 4013,
                containment: '.roadmap-frame-content',
                cursor: 'move'
            });
        },

        _onDragStop: function(e, ui) {
            var posx = ui.position.left - Roadmap.LANE_TITLE_WIDTH;
            var timeline = this.options.roadmap._timelineView.model.attributes;
            var markerDate;
            if (timeline.displayOption === Roadmap.TIMELINE_DISPLAY_OPTION.MONTH) {
                markerDate = Confluence.Roadmap.Helper.getMonthStartDateByPosition(timeline, posx);
            } else {
                markerDate = Confluence.Roadmap.Helper.getWeekStartDateByPosition(timeline, posx);
            }

            this.model.set('markerDate', markerDate);
        },

        _onDragStart: function(e, ui) {
            this.options.roadmap.hideDialog();
        },

        _onMarkerClick: function(e) {
            if (!$(e.target).hasClass('marker-title')) {
                return;
            }
            this._markerDialog && this._markerDialog.remove();
            this._markerDialog = this._createMarkerDialog();
            this._markerDialog.show();
        },

        _createMarkerDialog: function() {
            return new Roadmap.MarkerDialogView({
                trigger: this.$el.find(".marker-title"),
                marker: this
            });
        },

        updateHeight: function(timelineHeight) {
            this.$el.height(timelineHeight + Roadmap.MARKER_HEIGHT_PADDING);
        },

        renameTitle: function(newTitle) {
            this.model.set('title', newTitle);
            var $markerTitle = this.$el.find('.marker-title');
            $markerTitle.attr('title', newTitle);
            $markerTitle.html(AJS.escapeHtml(newTitle)).ellipsis({row: Roadmap.MARKER_TITLE_LINE});
        },

        removeMarker: function() {
            this.model.collection.remove(this.model);
            this.remove();
        }
    });
})(AJS.$, window._);