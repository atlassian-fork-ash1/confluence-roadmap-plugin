(function($, _) {
    'use strict';

    window.Roadmap = window.Roadmap || {};
    var RM = {};
    RM.Helper = Confluence.Roadmap.Helper;

    Roadmap.TimelineView = Backbone.View.extend({
        className: 'roadmap-frame-title',

        initialize: function() {
            _.bindAll(this, 'render');
        },

        render: function() {
            this.model.calculateTimelineColumns();
            this.$el.html(Confluence.Templates.Roadmap.timeline({displayTimelines: this.model.get('displayTimelineColumns')}));
            return this;
        },

        update: function() {
            this.render();
        },

        getBarPosition: function(bar) {
            if (this.model.get('displayOption') === Roadmap.TIMELINE_DISPLAY_OPTION.MONTH) {
                return {
                    left: RM.Helper.getPosXOnMonthTimeline(this.model.attributes, bar.startDate),
                    width: RM.Helper.getWidthTimeline(this.model.attributes, bar)
                };
            }

            return {
                left: RM.Helper.getPosXOnWeekTimeline(this.model.attributes, bar.startDate),
                width: RM.Helper.getWidthTimeline(this.model.attributes, bar)
            };
        },

        getBarDuration: function(barWidth) {
            return (this.model.get('displayOption') === Roadmap.TIMELINE_DISPLAY_OPTION.MONTH)
                ? (barWidth / Roadmap.MONTH_WIDTH)
                : (barWidth / Roadmap.WEEK_WIDTH);
        },

        getBarStartDate: function(positionX) {
            return (this.model.get('displayOption') === Roadmap.TIMELINE_DISPLAY_OPTION.MONTH)
                ? RM.Helper.getMonthStartDateByPosition(this.model.attributes, positionX)
                : RM.Helper.getWeekStartDateByPosition(this.model.attributes, positionX);
        }
    });
})(AJS.$, window._);