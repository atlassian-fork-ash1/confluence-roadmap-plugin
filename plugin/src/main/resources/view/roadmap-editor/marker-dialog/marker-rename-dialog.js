(function($, _) {
    var FormStateControl = require('confluence/form-state-control');

    var InlineDialogView = Roadmap.InlineDialogView;
    Roadmap.MarkerRenameDialogView = InlineDialogView.extend({
        events: {
            'click .rename-button': '_onSaveButtonClick',
            'keyup .marker-title': '_onChangeMarkerTitle'
        },

        initialize: function() {
            this.options.dialogOptions = {
                width: 315
            };
            InlineDialogView.prototype.initialize.call(this, this.options);
        },

        _getContent: function(element) {
            element.html(Confluence.Templates.Roadmap.markerRenameDialog({markerTitle: this.options.marker.model.get('title')}));
            this.controls = {
                $renameButton: this.$el.find('.rename-button'),
                $title: this.$el.find('.marker-title')
            };
        },

        _onChangeMarkerTitle: function(e) {
            var changedTitle = $.trim(this.controls.$title.val());
            if (!changedTitle) {
                FormStateControl.disableElement(this.controls.$renameButton);
            } else {
                FormStateControl.enableElement(this.controls.$renameButton);
                if (e.which == AJS.$.ui.keyCode.ENTER) {
                    this.controls.$renameButton.click();
                }
            }
        },

        _onShow: function(e) {
            this.controls.$title.select();
        },

        _onSaveButtonClick: function(e) {
            e.preventDefault();
            this.hide();
            this.options.marker.renameTitle(this.controls.$title.val());
        }
    });
})(AJS.$, window._);