(function($, _) {
    var InlineDialogView = Roadmap.InlineDialogView;
    Roadmap.MarkerDialogView = InlineDialogView.extend({
        events: {
            'click #rename-button': '_onRenameButtonClick',
            'click #delete-button': '_onDeleteButtonClick'
        },

        initialize: function() {
            this.options.dialogOptions = {
                width: 60
            };
            InlineDialogView.prototype.initialize.call(this, this.options);
        },

        _getContent: function(element) {
            element.html(Confluence.Templates.Roadmap.markerDialog());
            this.$el.find('.aui-button.aui-button-compact').tooltip({gravity: 's'});
        },

        _onRenameButtonClick: function() {
            if (!this._markerRenameDialog) {
                this._markerRenameDialog = new Roadmap.MarkerRenameDialogView({
                    trigger: this.options.trigger,
                    marker: this.options.marker
                });
            }
            this._markerRenameDialog.show();
            this.hide();
        },

        _onDeleteButtonClick: function() {
            this.options.marker.removeMarker();
            this.hide();
        }
    });
})(AJS.$, window._);