(function() {
    window.Roadmap = window.Roadmap || {};
    Roadmap.Bars = Backbone.Collection.extend({
        model: Roadmap.Bar
    });

    Roadmap.Lanes = Backbone.Collection.extend({
        model: Roadmap.Lane
    });

    Roadmap.Markers = Backbone.Collection.extend({
        model: Roadmap.Marker
    });
})();