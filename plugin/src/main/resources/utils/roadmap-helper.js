(function($, _) {
    var RM = Confluence.Roadmap = Confluence.Roadmap || {};

    RM.Helper = {
        getPosXOnMonthTimeline: function(timeline, date) {
            var mStartDateTimeline = moment(timeline.startDate).startOf('month');
            var mDateToBeginRender = moment(date).startOf('month');

            var months = RM.DateUtilities.getNumberOfMonths(mStartDateTimeline, mDateToBeginRender);
            var posX = months * Roadmap.MONTH_WIDTH; // posX base on month period
            var pixelFromStartMonth = RM.DateUtilities.getMilisecondsFromStartMonth(date) * Roadmap.MONTH_WIDTH / RM.DateUtilities.getMilisecondsInMonth(date);
            return Math.round(posX + pixelFromStartMonth);
        },

        /**
         * Get CSS left position for a bar by start date of timeline and start date of the bar.
         *
         * @param {Object} timeline
         * @param {Date} timeline.startDate - start date of timeline
         * @param {Date} timeline.endDate - end date of timeline
         * @param {string} timeline.displayOption - WEEK or MONTH
         *
         * @param {Array} timeline.displayTimelineColumns
         * @param {Date} startDate - start date object of a bar
         * @returns {number} CSS left position, is negative number if timeline start day is after given bar start day.
         */
        getPosXOnWeekTimeline: function(timeline, startDate) {
            var mStartDateTimelineStartFromMonday = moment(timeline.startDate).startOf('isoWeek');
            var weeks = moment(startDate).diff(mStartDateTimelineStartFromMonday, 'weeks', true);
            var posX = weeks * Roadmap.WEEK_WIDTH;
            return Math.round(posX);
        },

        getWidthTimeline: function(timeline, bar) {
            var barWidth = bar.duration;
            if (timeline.displayOption === Roadmap.TIMELINE_DISPLAY_OPTION.MONTH) {
                barWidth *= Roadmap.MONTH_WIDTH;
            } else {
                barWidth *= Roadmap.WEEK_WIDTH;
            }

            return barWidth < Roadmap.EXTRA_SPACE_AROUND
                    ? 0
                    : Math.round(barWidth - Roadmap.EXTRA_SPACE_AROUND);
        },

        getMonthStartDateByPosition: function(timeline, positionX) {
            var months = Math.floor(positionX / Roadmap.MONTH_WIDTH);
            var mTimelineStartDate = moment(timeline.startDate).startOf('month');
            var date = mTimelineStartDate.add(months, 'months').toDate(); //add month

            var daysWidth = Math.abs(positionX) % Roadmap.MONTH_WIDTH;
            if (daysWidth !== 0 && months < 0) {
                daysWidth = Roadmap.MONTH_WIDTH - daysWidth;
            }

            var numberOfMilisecondFromMonth = RM.DateUtilities.getMilisecondsInMonth(date) * daysWidth / Roadmap.MONTH_WIDTH;
            return new Date(date.getTime() + numberOfMilisecondFromMonth);
        },

        getWeekStartDateByPosition: function(timeline, positionX) {
            var mStartDateTimeline = moment(timeline.startDate).startOf('isoWeek');

            var numberOfWeeks = positionX / Roadmap.WEEK_WIDTH;
            var milliSeconds = numberOfWeeks * RM.DateUtilities.MILLISECONDS_A_WEEK;

            return new Date(mStartDateTimeline.valueOf() + milliSeconds);
        },

        guid: function() {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        }
    };

    RM.ColorHelper = {
        resetColorCounts: function() {
            _.each(Roadmap.COLORS, function(color) {
                color.count = 0;
            });
        },

        findColor: function(color) {
            return _.find(Roadmap.COLORS, function(roadmapColor) {
                return roadmapColor.lane === color.lane;
            });
        },

        adjustColorCount: function(color, adjustAmount) {
            var roadmapColor = RM.ColorHelper.findColor(color);
            if (roadmapColor) {
                roadmapColor.count = roadmapColor.count + adjustAmount;
            }
        },

        getColor: function() {
            return _.min(Roadmap.COLORS, function(color) {
                return color.count;
            });
        }
    };
}(AJS.$, window._));