package com.atlassian.plugins.roadmap;

import com.atlassian.confluence.links.linktypes.PageLink;
import com.atlassian.plugins.roadmap.models.RoadmapPageLink;
import com.atlassian.util.concurrent.Lazy;
import com.atlassian.util.concurrent.Supplier;
import com.atlassian.vcache.ExternalCacheSettingsBuilder;
import com.atlassian.vcache.Marshaller;
import com.atlassian.vcache.MarshallerException;
import com.atlassian.vcache.StableReadExternalCache;
import com.atlassian.vcache.VCacheFactory;

import com.atlassian.plugins.roadmap.TimelinePlannerMacroManager.LinkStatus;

import javax.annotation.Nonnull;

import java.time.Duration;

import static com.atlassian.vcache.marshallers.MarshallerFactory.serializableMarshaller;
import static com.atlassian.vcache.marshallers.MarshallerFactory.stringMarshaller;

class RoadmapMacroCacheSupplier {

    private final Supplier<StableReadExternalCache<byte[]>> imageCacheRef;
    private final Supplier<StableReadExternalCache<String>> macroSourceCacheRef;
    private final Supplier<StableReadExternalCache<LinkStatus>> linkStatusCacheRef;
    private final Supplier<StableReadExternalCache<RoadmapPageLink>> pageLinkCacheRef;

    public static final String IMAGE_CACHE_NAME = "RoadmapMacroImages";
    public static final String MACRO_SOURCE_CACHE_NAME = "RoadmapMacroSources";
    public static final String LINK_STATUS_CACHE_NAME = "RoadmapMacroLinkStatuses";
    public static final String PAGE_LINK_CACHE_NAME = "RoadmapMacroPageLinks";

    RoadmapMacroCacheSupplier(VCacheFactory cacheFactory) {
        this.imageCacheRef = Lazy.supplier(() -> createImageCache(cacheFactory));
        this.macroSourceCacheRef = Lazy.supplier(() -> createMacroSourceCache(cacheFactory));
        this.linkStatusCacheRef = Lazy.supplier(() -> createLinkStatusCache(cacheFactory));
        this.pageLinkCacheRef = Lazy.supplier(() -> createPageLinkCache(cacheFactory));
    }

    private static StableReadExternalCache<byte[]> createImageCache(VCacheFactory cacheFactory) {
        Marshaller<byte[]> marshaller = new ByteArrayMarshaller();

        return cacheFactory.getStableReadExternalCache(
                IMAGE_CACHE_NAME,
                marshaller,
                new ExternalCacheSettingsBuilder().build()
        );
    }

    private static StableReadExternalCache<String> createMacroSourceCache(VCacheFactory cacheFactory) {
        return cacheFactory.getStableReadExternalCache(
                MACRO_SOURCE_CACHE_NAME,
                stringMarshaller(),
                new ExternalCacheSettingsBuilder().build()
        );
    }

    private static StableReadExternalCache<LinkStatus> createLinkStatusCache(VCacheFactory cacheFactory) {
        return cacheFactory.getStableReadExternalCache(
                LINK_STATUS_CACHE_NAME,
                serializableMarshaller(LinkStatus.class),
                new ExternalCacheSettingsBuilder().defaultTtl(Duration.ofHours(3)).build()
        );
    }

    private static StableReadExternalCache<RoadmapPageLink> createPageLinkCache(VCacheFactory cacheFactory) {
        return cacheFactory.getStableReadExternalCache(
                PAGE_LINK_CACHE_NAME,
                serializableMarshaller(RoadmapPageLink.class),
                new ExternalCacheSettingsBuilder().defaultTtl(Duration.ofHours(3)).build()
        );
    }

    public StableReadExternalCache<byte[]> getImageCache() {
        return imageCacheRef.get();
    }

    public StableReadExternalCache<String> getMarcoSourceCache() {
        return macroSourceCacheRef.get();
    }

    public StableReadExternalCache<LinkStatus> getLinkStatusCache() {
        return linkStatusCacheRef.get();
    }

    public StableReadExternalCache<RoadmapPageLink> getPageLinkCache() {
        return pageLinkCacheRef.get();
    }

    public static class ByteArrayMarshaller implements Marshaller<byte[]> {
        @Nonnull
        @Override
        public byte[] marshall(byte[] obj) throws MarshallerException {
            return obj;
        }

        @Nonnull
        @Override
        public byte[] unmarshall(byte[] raw) throws MarshallerException {
            return raw;
        }
    }

}
