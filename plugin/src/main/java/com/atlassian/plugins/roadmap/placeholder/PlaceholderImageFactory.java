package com.atlassian.plugins.roadmap.placeholder;

import com.atlassian.sal.api.message.I18nResolver;

import java.awt.Font;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Image;
import java.awt.image.AreaAveragingScaleFilter;
import java.awt.image.FilteredImageSource;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;
import org.j3d.util.ImageGenerator;

public class PlaceholderImageFactory
{
    private static final String TITLE_LANGUAGE_KEY = "com.atlassian.confluence.plugins.confluence-roadmap-plugin.roadmap.label";
    private static final String ICON_PATH = "images/roadmap.png";

    private static final Color PLACEHOLDER_BACKGROUND = new Color(0xF0, 0xF0, 0xF0);
    private static final Color COLOR_PLACEHOLDER = Color.BLACK;

    private static final int MACRO_ICON_SIZE = 24;
    private static final int HORIZONTAL_PADDING = 14;
    private static final int VERTICAL_PADDING = 14;

    public static void drawPlaceholderImage(Graphics2D graphics, Font font, I18nResolver i18nResolver)
    {
        String macroTitle = i18nResolver.getText(TITLE_LANGUAGE_KEY);

        final InputStream iconStream = PlaceholderImageFactory.class.getClassLoader().getResourceAsStream(ICON_PATH);

        Font placeholderFont = font.deriveFont(Font.PLAIN, 13);
        FontMetrics placeholderFontMetrics = graphics.getFontMetrics(placeholderFont);

        int textHeight = placeholderFontMetrics.getMaxAscent() + placeholderFontMetrics.getMaxDescent();

        int placeholderImageHeight = textHeight + VERTICAL_PADDING;

        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics.setColor(PLACEHOLDER_BACKGROUND);

        Image macroIconImage = getIcon(iconStream, MACRO_ICON_SIZE);
        int macroIconImageWidth = macroIconImage.getWidth(null);
        int macroIconImageHeight = macroIconImage.getHeight(null);
        int macroIconOffsetX = 0;
        int macroIconOffsetY = (placeholderImageHeight - macroIconImageHeight) / 2;
        graphics.drawImage(macroIconImage, macroIconOffsetX, macroIconOffsetY, macroIconImageWidth, macroIconImageHeight, null);

        // Draw text
        graphics.setFont(placeholderFont);
        float fontMidlineToBaselineOffset = placeholderFontMetrics.getMaxAscent() - (textHeight / 2f);
        float baselineOffset = (VERTICAL_PADDING + textHeight) / 2f + fontMidlineToBaselineOffset;

        float macroTitleTextOffsetX = HORIZONTAL_PADDING / 2f + MACRO_ICON_SIZE;
        float currentOffsetX = macroTitleTextOffsetX;

        graphics.setColor(COLOR_PLACEHOLDER);
        graphics.drawString(macroTitle, currentOffsetX, baselineOffset);
    }

    private static Image getIcon(InputStream inputStream, int iconSize)
    {
        Image result;
        try
        {
            result = ImageIO.read(inputStream);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }

        if (result.getWidth(null) > iconSize || result.getHeight(null) > iconSize)
        {
            AreaAveragingScaleFilter scaleFilter = new AreaAveragingScaleFilter(iconSize, iconSize);
            FilteredImageSource filteredImage = new FilteredImageSource(result.getSource(), scaleFilter);
            ImageGenerator generator = new ImageGenerator();
            filteredImage.startProduction(generator);
            result = generator.getImage();
            result.flush();
        }

        return result;
    }
}
