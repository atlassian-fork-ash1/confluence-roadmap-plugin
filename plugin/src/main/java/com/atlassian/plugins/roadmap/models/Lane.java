package com.atlassian.plugins.roadmap.models;

import java.util.List;

public class Lane
{
    private String title;
    private LaneColor color;
    private List<Bar> bars;

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public LaneColor getColor()
    {
        return color;
    }

    public void setColor(LaneColor colour)
    {
        this.color = colour;
    }

    public List<Bar> getBars()
    {
        return bars;
    }

    public void setBars(List<Bar> bars)
    {
        this.bars = bars;
    }
}
