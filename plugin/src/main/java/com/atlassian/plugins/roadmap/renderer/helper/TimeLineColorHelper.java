package com.atlassian.plugins.roadmap.renderer.helper;

import java.awt.*;

/**
 * Java2D color helper
 */
public class TimeLineColorHelper
{
    /**
     * Verify if two color are contrasted together
     * @param c1 color 1
     * @param c2 color 2
     * @return {@code true} if contrast
     */
    public static boolean isContrasted(Color c1, Color c2)
    {
        double L1 = getLuminosity(c1);
        double L2 = getLuminosity(c2);

        double contrast = (L1 + 0.05) / (L2 + 0.05);

        return contrast > 0.5;
    }

    /**
     * Convert a html color code to awt color
     * @param color color
     * @return {Color}
     */
    public static Color decodeColor(String color)
    {
        // Decode hex
        if (color.startsWith("#"))
        {
            return new Color(Integer.decode("0x" + color.substring(1)));
        }
        return new Color(Integer.decode("0x" + color));
    }

    private static double getLuminosity(Color color)
    {
        return (0.2126 * getLinearisedColor(color.getRed()) +
                0.7152 * getLinearisedColor(color.getGreen()) +
                0.0722 * getLinearisedColor(color.getBlue()));
    }

    private static double getLinearisedColor(int component)
    {
        return Math.pow(component / 0xFF, 2.2);
    }
}
