package com.atlassian.plugins.roadmap;

import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.plugins.roadmap.models.TimelinePlanner;
import com.atlassian.plugins.roadmap.renderer.PNGRoadMapRenderer;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.vcache.PutPolicy;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.Optional;

import static com.atlassian.vcache.VCacheUtils.unsafeJoin;


public class ImageServlet extends HttpServlet {
    private final Logger logger = LoggerFactory.getLogger(ImageServlet.class);

    private TimelinePlannerMacroManager timelinePlannerMacroManager;
    private final I18nResolver i18n;
    private final RoadmapMacroCacheSupplier cacheSupplier;


    public ImageServlet(I18nResolver i18nResolver, RoadmapMacroCacheSupplier cacheSupplier, TimelinePlannerMacroManager timelinePlannerMacroManager) {
        this.i18n = i18nResolver;
        this.timelinePlannerMacroManager = timelinePlannerMacroManager;
        this.cacheSupplier = cacheSupplier;
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        RoadmapRequest roadmapRequest = new RoadmapRequest(req);

        if (!isValid(roadmapRequest)) {
            logger.error("Roadmap ImageServlet - roadmap's request is invalid");
            responseDefaultRoadmap(resp);
            return;
        }

        String imageCacheKey = String.format("%s_%s_%s", roadmapRequest.getHash(), roadmapRequest.getWidth(), roadmapRequest.getHeight());
        Optional<byte[]> roadmapImage = unsafeJoin(cacheSupplier.getImageCache().get(imageCacheKey));

        // If there's no cached image, go ahead and render it
        if (!roadmapImage.isPresent()) {
            roadmapImage = renderRoadmap(roadmapRequest);

            // If we've managed to render the roadmap, save it in the cache
            roadmapImage.ifPresent((image) -> {
                unsafeJoin(cacheSupplier.getImageCache().put(imageCacheKey, image, PutPolicy.PUT_ALWAYS));
            });
        }

        if (roadmapImage.isPresent()) {
            resp.setContentType("image/png");
            resp.getOutputStream().write(roadmapImage.get());
        } else {
            responseDefaultRoadmap(resp);
        }
    }

    private Optional<byte[]> renderRoadmap(RoadmapRequest roadmapRequest) throws IOException {
        Optional<String> source;
        boolean isTimeline;

        if (roadmapRequest.getId() > 0 && roadmapRequest.getVersion() > 0) {
            try {
                MacroDefinition roadmapMacro =
                        timelinePlannerMacroManager.findRoadmapMacroDefinition(roadmapRequest.getId(), roadmapRequest.getVersion(), roadmapRequest.getHash());
                if (roadmapMacro == null) {
                    source = Optional.empty();
                    isTimeline = false;
                } else {
                    source = Optional.ofNullable(roadmapMacro.getParameter("source"));
                    isTimeline = BooleanUtils.toBoolean(roadmapMacro.getParameter("timeline"));
                }
            } catch (XhtmlException e) {
                logger.error("Roadmap source error: ", e);
                throw new IOException(e);
            }
        } else {
            // This cache is populated when the placeholder image is generated (see RoadmapMacro#generateImagePlaceholder)
            source = unsafeJoin(cacheSupplier.getMarcoSourceCache().get(roadmapRequest.getHash()));
            isTimeline = roadmapRequest.isTimeline();
        }

        if (source.isPresent()) {
            BufferedImage bufferedImage;

            Optional<Integer> widthOption = Optional.empty();
            Optional<Integer> heightOption = Optional.empty();
            if (roadmapRequest.getWidth() > 0 && roadmapRequest.getHeight() > 0) {
                widthOption = Optional.of(roadmapRequest.getWidth());
                heightOption = Optional.of(roadmapRequest.getHeight());
            }

            if (isTimeline) {
                TimelinePlanner roadmap = TimelinePlannerJsonBuilder.fromJson(source.get());
                PNGRoadMapRenderer pngRoadMapRenderer = new PNGRoadMapRenderer();
                pngRoadMapRenderer.setI18n(this.i18n);
                bufferedImage = pngRoadMapRenderer.renderAsImage(roadmap, widthOption, heightOption, roadmapRequest.isPlaceholder());
            } else {
                bufferedImage = RoadmapRenderer.drawImage(URLDecoder.decode(source.get(), "UTF-8"), widthOption,
                        heightOption, roadmapRequest.isPlaceholder(), i18n);
            }

            ByteArrayOutputStream buf = new ByteArrayOutputStream();
            ImageIO.write(bufferedImage, "png", buf);

            return Optional.of(buf.toByteArray());
        } else {
            return Optional.empty();
        }
    }

    private void responseDefaultRoadmap(HttpServletResponse resp) throws IOException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("images/roadmap.png");
        resp.setContentType("image/png");
        IOUtils.copy(inputStream, resp.getOutputStream());
        inputStream.close();
    }

    private boolean isValid(RoadmapRequest roadmapRequest) {
        return StringUtils.isNotBlank(roadmapRequest.getHash());
    }
}