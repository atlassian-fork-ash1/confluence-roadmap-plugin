package com.atlassian.plugins.roadmap.upgradetask;

import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * CONFDEV-31726: This upgrade task remove "currentspaces" parameter from Roadmap macros.
 *
 * @see CurrentSpacesParamMigrator for details.
 * @see com.atlassian.confluence.upgrade.upgradetask.JiraIssueMacroServerParamsUpgradeTask for reference
 */

public class CurrentSpacesParamUpgradeTask implements PluginUpgradeTask
{
    private static final Logger log = LoggerFactory.getLogger(CurrentSpacesParamUpgradeTask.class);
    private final CurrentSpacesParamMigrator migrator;

//    @Autowired
    public CurrentSpacesParamUpgradeTask(CurrentSpacesParamMigrator migrator)
    {
        this.migrator = migrator;
    }

    @Override
    public int getBuildNumber()
    {
        return 6;
    }

    @Override
    public String getShortDescription()
    {
        return "Remove the \"currentspaces\" parameter from Roadmap macros";
    }

    @Override
    public Collection<Message> doUpgrade() throws Exception
    {
        log.info("Starting to migrate Roadmap macros");
        ConfluenceUser currentUser = AuthenticatedUserThreadLocal.get();
        migrator.migrate();
        AuthenticatedUserThreadLocal.set(currentUser);
        return null;
    }

    @Override
    public String getPluginKey()
    {
        return "com.atlassian.confluence.plugins.confluence-roadmap-plugin";
    }
}