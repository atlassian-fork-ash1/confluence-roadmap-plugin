package com.atlassian.plugins.roadmap.renderer;

import com.atlassian.plugins.roadmap.models.Bar;
import com.atlassian.plugins.roadmap.models.Lane;
import com.atlassian.plugins.roadmap.models.LaneColor;

import java.util.Calendar;
import java.util.Date;

import static java.util.Arrays.asList;

/**
 * Provide some factory function to initialize Roadmap model
 *
 */
public class RoadmapDataTestingHelper
{
    public static Calendar createCalendar(int day, int month, int year)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.YEAR, year);
        return calendar;
    }

    public static Date getDateWithMonth(Calendar calendar, int month)
    {
        Calendar cloneCalendar = (Calendar) calendar.clone();
        cloneCalendar.add(Calendar.MONTH, month);
        return cloneCalendar.getTime();
    }

    public static Lane createLane(String title, LaneColor laneClor, Bar... bars)
    {
        Lane lane = new Lane();
        lane.setTitle("Lane1");
        lane.setColor(laneClor);
        lane.setBars(asList(bars));
        return lane;
    }

    public static Bar createBar(int rowIndex, double duration, String title, Date startDate)
    {
        Bar bar = new Bar();
        bar.setRowIndex(rowIndex);
        bar.setTitle(title);
        bar.setDuration(duration);
        bar.setStartDate(startDate);
        return bar;
    }

}
