package com.atlassian.plugins.roadmap.renderer;

import com.atlassian.plugins.roadmap.models.Bar;
import com.atlassian.plugins.roadmap.models.Lane;
import com.atlassian.plugins.roadmap.models.LaneColor;
import com.atlassian.plugins.roadmap.models.Marker;
import com.atlassian.plugins.roadmap.models.Timeline;
import com.atlassian.plugins.roadmap.models.Timeline.DisplayOption;
import com.atlassian.plugins.roadmap.models.TimelinePlanner;
import com.atlassian.sal.api.message.I18nResolver;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.atlassian.plugins.roadmap.renderer.NumberOfStringMatchers.hasNumberOfSubstrings;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class SVGRoadMapRendererTest
{
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private I18nResolver i18nResolver;

    @Before
    public void setUp()
    {
        mockI18n();
    }

    @After
    public void tearDown()
    {
        i18nResolver = null;
    }

    private TimelinePlanner createTimelinePlanner()
    {
        Calendar calendar = RoadmapDataTestingHelper.createCalendar(1, 3, 2014);

        TimelinePlanner timelinePlanner = new TimelinePlanner();

        timelinePlanner.setTitle("TimelinePlanner");
        timelinePlanner.setTimeline(new Timeline());
        timelinePlanner.setMarkers(new ArrayList<>());
        timelinePlanner.setLanes(new ArrayList<>());

        // timeline
        Timeline timeline = timelinePlanner.getTimeline();
        timeline.setDisplayOption(DisplayOption.MONTH);
        timeline.setStartDate(RoadmapDataTestingHelper.getDateWithMonth(calendar, 0));
        timeline.setEndDate(RoadmapDataTestingHelper.getDateWithMonth(calendar, 11));

        // markers
        List<Marker> markers = timelinePlanner.getMarkers();
        Marker marker1 = new Marker();
        marker1.setTitle("Marker1");
        marker1.setMarkerDate(RoadmapDataTestingHelper.getDateWithMonth(calendar, 9));
        markers.add(marker1);

        // lanes and bar
        Bar bar1 = RoadmapDataTestingHelper.createBar(0, 2, "Bar1", RoadmapDataTestingHelper.getDateWithMonth(calendar, 1));
        Lane lane1 = RoadmapDataTestingHelper.createLane("Lane1", new LaneColor("#11111", "#eeeeee", "#33333"), bar1);
        timelinePlanner.getLanes().add(lane1);

        return timelinePlanner;
    }

    private void mockI18n()
    {
        when(i18nResolver.getText("roadmap.editor.timeline.month1")).thenReturn("Jan");
        when(i18nResolver.getText("roadmap.editor.timeline.month2")).thenReturn("Feb");
        when(i18nResolver.getText("roadmap.editor.timeline.month3")).thenReturn("Mar");
        when(i18nResolver.getText("roadmap.editor.timeline.month4")).thenReturn("Apr");
        when(i18nResolver.getText("roadmap.editor.timeline.month5")).thenReturn("May");
        when(i18nResolver.getText("roadmap.editor.timeline.month6")).thenReturn("Jun");
        when(i18nResolver.getText("roadmap.editor.timeline.month7")).thenReturn("Jul");
        when(i18nResolver.getText("roadmap.editor.timeline.month8")).thenReturn("Aug");
        when(i18nResolver.getText("roadmap.editor.timeline.month9")).thenReturn("Sep");
        when(i18nResolver.getText("roadmap.editor.timeline.month10")).thenReturn("Oct");
        when(i18nResolver.getText("roadmap.editor.timeline.month11")).thenReturn("Nov");
        when(i18nResolver.getText("roadmap.editor.timeline.month12")).thenReturn("Dec");
    }

    @Test
    public void renderAsString() throws IOException
    {
        TimelinePlanner roadmap = createTimelinePlanner();
        SVGRoadMapRenderer svgRoadMapRenderer = new SVGRoadMapRenderer();
        svgRoadMapRenderer.setI18n(i18nResolver);
        String svgXmlCode = svgRoadMapRenderer.renderAsString(roadmap);

        assertThat(svgXmlCode, hasNumberOfSubstrings(">2014<", 1));
        assertThat(svgXmlCode, hasNumberOfSubstrings(">2015<", 1));

        assertThat(svgXmlCode, hasNumberOfSubstrings(">Mar<", 1));
        assertThat(svgXmlCode, hasNumberOfSubstrings(">Feb<", 1));

        assertThat(svgXmlCode, containsString("Lane1"));
        assertThat(svgXmlCode, containsString("Bar1"));
        assertThat(svgXmlCode, containsString("Marker1"));
    }

    @Test
    public void renderWeekRoadmap() throws IOException
    {
        TimelinePlanner roadmap = createWeekRoadmap();
        SVGRoadMapRenderer svgRoadMapRenderer = new SVGRoadMapRenderer();
        svgRoadMapRenderer.setI18n(i18nResolver);
        String svgXmlCode = svgRoadMapRenderer.renderAsString(roadmap);

        // verify week timeline display
        assertThat(svgXmlCode, hasNumberOfSubstrings(">2015<", 1));
        assertThat(svgXmlCode, hasNumberOfSubstrings(">2016<", 1));

        assertThat(svgXmlCode, containsString("23-Feb"));
        assertThat(svgXmlCode, containsString("02-Mar"));
        assertThat(svgXmlCode, containsString("30-Mar"));
        assertThat(svgXmlCode, containsString("06-Apr"));
        assertThat(svgXmlCode, containsString("27-Apr"));
        assertThat(svgXmlCode, containsString("04-May"));
        assertThat(svgXmlCode, containsString("25-May"));
        assertThat(svgXmlCode, containsString("01-Jun"));
        assertThat(svgXmlCode, containsString("29-Jun"));
        assertThat(svgXmlCode, containsString("06-Jul"));
        assertThat(svgXmlCode, containsString("27-Jul"));
        assertThat(svgXmlCode, containsString("03-Aug"));
        assertThat(svgXmlCode, containsString("31-Aug"));
        assertThat(svgXmlCode, containsString("07-Sep"));
        assertThat(svgXmlCode, containsString("28-Sep"));
        assertThat(svgXmlCode, containsString("05-Oct"));
        assertThat(svgXmlCode, containsString("26-Oct"));
        assertThat(svgXmlCode, containsString("02-Nov"));
        assertThat(svgXmlCode, containsString("30-Nov"));
        assertThat(svgXmlCode, containsString("07-Dec"));
        assertThat(svgXmlCode, containsString("28-Dec"));
        assertThat(svgXmlCode, containsString("04-Jan"));
        assertThat(svgXmlCode, containsString("25-Jan"));
        assertThat(svgXmlCode, containsString("01-Feb"));

        assertThat(svgXmlCode, containsString("Lane1"));
        assertThat(svgXmlCode, containsString("Bar1"));
        assertThat(svgXmlCode, containsString("Marker1"));
    }

    private TimelinePlanner createWeekRoadmap()
    {
        Calendar calendar = RoadmapDataTestingHelper.createCalendar(1, 3, 2015);
        TimelinePlanner timelinePlanner = new TimelinePlanner();
        timelinePlanner.setTimeline(new Timeline());
        timelinePlanner.setLanes(new ArrayList<>());
        timelinePlanner.setMarkers(new ArrayList<>());

        Timeline timeline = timelinePlanner.getTimeline();
        timeline.setDisplayOption(DisplayOption.WEEK);
        timeline.setStartDate(RoadmapDataTestingHelper.getDateWithMonth(calendar, 0));
        timeline.setEndDate(RoadmapDataTestingHelper.getDateWithMonth(calendar, 11));

        // lanes and bars
        List<Lane> lanes = timelinePlanner.getLanes();
        Bar bar1 = RoadmapDataTestingHelper.createBar(0, 2, "Bar1", RoadmapDataTestingHelper.getDateWithMonth(calendar, 1));
        Lane lane1 = RoadmapDataTestingHelper.createLane("Lane1", new LaneColor("#11111", "#eeeeee", "#33333"), bar1);
        lanes.add(lane1);

        // markers
        List<Marker> markers = timelinePlanner.getMarkers();
        Marker marker1 = new Marker();
        marker1.setTitle("Marker1");
        marker1.setMarkerDate(RoadmapDataTestingHelper.getDateWithMonth(calendar, 9));
        markers.add(marker1);

        return timelinePlanner;
    }
}
