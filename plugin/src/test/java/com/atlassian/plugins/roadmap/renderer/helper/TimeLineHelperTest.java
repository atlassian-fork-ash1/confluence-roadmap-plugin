package com.atlassian.plugins.roadmap.renderer.helper;


import com.atlassian.plugins.roadmap.models.Timeline;
import com.atlassian.plugins.roadmap.renderer.RoadmapDataTestingHelper;
import com.atlassian.plugins.roadmap.renderer.beans.TimelinePosition;
import org.junit.Test;

import java.util.Calendar;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TimeLineHelperTest
{
    @Test
    public void calculateTimelinePosition()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.MONTH, Calendar.APRIL);
        calendar.set(Calendar.YEAR, 2014);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        Timeline timeline = new Timeline();
        timeline.setDisplayOption(Timeline.DisplayOption.MONTH);

        timeline.setStartDate(RoadmapDataTestingHelper.getDateWithMonth(calendar, 0));
        timeline.setEndDate(RoadmapDataTestingHelper.getDateWithMonth(calendar, 11));

        TimelinePosition timelinePosition = TimeLineHelper.calculateTimelinePosition(timeline, RoadmapDataTestingHelper.getDateWithMonth(calendar, 3));

        double approximatePositionAfterThreeMonths = round(timelinePosition.getColumn() + timelinePosition.getOffset(), 1);
        assertThat(approximatePositionAfterThreeMonths, is(3.0));

        TimelinePosition timelinePosition2 = TimeLineHelper.calculateTimelinePosition(timeline, RoadmapDataTestingHelper.getDateWithMonth(calendar, 6));
        double approximatePositionAfterSixMonths = round(timelinePosition2.getColumn() + timelinePosition2.getOffset(), 1);
        assertThat(approximatePositionAfterSixMonths, is(6.0));
    }

    // -----------------Private support function-----------------------------------------

    private double round(double d, int dyn)
    {
        double power = Math.pow(10, dyn);
        return Math.round(d * power) / power;
    }
}
