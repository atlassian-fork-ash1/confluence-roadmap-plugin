package it.com.atlassian.plugins.roadmap.pageobjects;

import com.atlassian.confluence.webdriver.pageobjects.component.blueprint.BlueprintDialog;
import com.atlassian.confluence.webdriver.pageobjects.component.dialog.Dialog;
import com.atlassian.confluence.webdriver.pageobjects.page.content.CreatePage;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import org.junit.Assert;
import org.openqa.selenium.By;

import java.util.Set;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.junit.Assert.assertTrue;

public class BarPageLinkDialog extends Dialog
{
    public BarPageLinkDialog()
    {
        super("inline-dialog-roadmap-dialog");
    }

    public String getPageLinkTitle()
    {
        PageElement linkPageTitle = getDialog().find(By.cssSelector(".link-page-title"), TimeoutType.PAGE_LOAD);
        return linkPageTitle.getText();
    }

    public BarPageLinkDialog createNewPageAndRebind(String createdPageTitle, String createdPageContent)
    {
        String baseWindow = driver.getWindowHandle();
        BlueprintDialog blueprintDialog = clickCreatePageLink();
        CreatePage createPage = blueprintDialog.submitForCreatePageEditor();
        createPage.setTitle(createdPageTitle);
        createPage.getEditor().getContent().setContent(createdPageContent);
        createPage.save();
        driver.close();

        driver.switchTo().window(baseWindow);
        driver.executeScript("window.dispatchEvent(new Event('focus'))"); //make sure driver always fires 'focus' event
        return pageBinder.bind(BarPageLinkDialog.class);
    }

    private BlueprintDialog clickCreatePageLink()
    {
        String parentWindow = driver.getWindowHandle();
        PageElement newPage = find(".link-new-page").click();
        waitUntilTrue(newPage.timed().isVisible());
        waiter.until(30).element(By.cssSelector(".create-dialog-create-button")).isEnabled();
        switchToNewPage(parentWindow);
        assertTrue("Create blank page must be appeared", isBlueprintItemPresent("com.atlassian.confluence.plugins.confluence-create-content-plugin:create-blank-page"));
        return pageBinder.bind(BlueprintDialog.class);
    }

    private void switchToNewPage(String parentWindow)
    {
        Set<String> set = driver.getWindowHandles();
        set.remove(parentWindow);
        String childWindow = (String) set.toArray()[0];
        Assert.assertNotEquals("Child page should be activated", parentWindow, childWindow);
        driver.switchTo().window(childWindow);
    }

    private boolean isBlueprintItemPresent(String blueprintModuleCompleteKey)
    {
        PageElement templateList = pageElementFinder.find(By.className("templates"), TimeoutType.SLOW_PAGE_LOAD);
        waitUntilTrue(templateList.timed().isVisible());
        return templateList.findAll(By.cssSelector("li[data-item-module-complete-key='" + blueprintModuleCompleteKey + "']")).size() > 0;
    }
}
