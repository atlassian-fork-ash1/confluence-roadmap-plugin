package it.com.atlassian.plugins.roadmap.pageobjects;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

public class BarDialog extends BarPageLinkDialog
{
    @ElementBy(id = "confluence-roadmap-dialog")
    private PageElement roadmapEditorDialog;

    @ElementBy(id = "delete-button")
    private PageElement deleteButton;

    @ElementBy(id = "bar-title")
    private PageElement barTitle;

    @ElementBy(name = "inplace_value")
    private PageElement barTitleInput;

    @ElementBy(cssSelector = ".bar-description")
    private PageElement barDescription;

    @ElementBy(cssSelector = ".bar-description .inplace_field.textarea")
    private PageElement barDescriptionInput;

    @ElementBy(id = "inline-edit-save")
    private PageElement saveButton;

    public BarDialog clickDelete()
    {
        deleteButton.click();
        return this;
    }

    public BarDialog clickRename()
    {
        barTitle.click();
        return this;
    }

    public BarDialog inputBarTitle(String title)
    {
        barTitleInput.clear();
        barTitleInput.type(title);
        return this;
    }

    public BarDialog clickDescription()
    {
        barDescription.click();
        return this;
    }

    public String getDescriptionText()
    {
        return barDescription.getText();
    }

    public BarDialog inputDescription(String description)
    {
        barDescriptionInput.clear();
        barDescriptionInput.type(description);
        return this;
    }

    public BarDialog clickSave()
    {
        saveButton.click();
        return this;
    }

    public boolean isSaveButtonEnabled()
    {
        return saveButton.isEnabled();
    }
}