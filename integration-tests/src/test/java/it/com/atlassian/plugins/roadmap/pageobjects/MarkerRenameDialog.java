package it.com.atlassian.plugins.roadmap.pageobjects;

import com.atlassian.confluence.webdriver.pageobjects.component.dialog.Dialog;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.Matchers.is;

public class MarkerRenameDialog extends Dialog
{
    @ElementBy(cssSelector = ".text.marker-title")
    private PageElement markerTitleText;

    @ElementBy(cssSelector = ".rename-button")
    private PageElement saveButton;

    public MarkerRenameDialog()
    {
        super("inline-dialog-roadmap-dialog");
    }

    public MarkerRenameDialog clickSave()
    {
        saveButton.click();
        return this;
    }

    public MarkerRenameDialog inputTextTitle(String title)
    {
        waitUntilTrue(markerTitleText.timed().isVisible());
        markerTitleText.clear();
        markerTitleText.type(title);
        waitUntil(markerTitleText.timed().getValue(), is(title));
        return this;
    }
}
