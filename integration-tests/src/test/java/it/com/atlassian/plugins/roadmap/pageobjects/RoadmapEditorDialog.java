package it.com.atlassian.plugins.roadmap.pageobjects;

import com.atlassian.confluence.webdriver.pageobjects.component.dialog.Dialog;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import it.com.atlassian.plugins.roadmap.utils.RoadmapStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public class RoadmapEditorDialog extends Dialog
{
    @ElementBy(id = "macro-roadmap", timeoutType=TimeoutType.SLOW_PAGE_LOAD)
    private PageElement roadmapMacroItem;

    @ElementBy(id = "confluence-roadmap-dialog")
    private PageElement roadmapEditorDialog;

    @ElementBy(id = "roadmapInsertBtn")
    private PageElement saveButton;

    @ElementBy(cssSelector = ".roadmap-frame-title")
    private PageElement timelineContainer;

    @ElementBy(id = "toolbar-button-add-bar")
    private PageElement addBarButton;

    @ElementBy(id = "timeline-startdate-input")
    private PageElement startDate;

    @ElementBy(id = "timeline-enddate-input")
    private PageElement endDate;

    public RoadmapEditorDialog()
    {
        super("confluence-roadmap-dialog");
    }

    @Override
    public boolean isVisible(){
        return isRoadmapEditorDialogVisible().now();
    }

    public RoadmapEditorDialog open()
    {
        waitUntilTrue(roadmapMacroItem.timed().isVisible());
        roadmapMacroItem.click();
        return this;
    }

    public TimedQuery<Boolean> isRoadmapEditorDialogVisible()
    {
        return roadmapEditorDialog.find(By.className("aui-dialog2-header")).timed().isVisible();
    }

    public void clickSave()
    {
        saveButton.click();
        waitUntilHidden();
    }

    public String getFirstTimeline()
    {
        List<String> timelines = getAllTimelinesText();
        return timelines.get(0);
    }

    public String getLastTimeline()
    {
        List<String> timelines = getAllTimelinesText();
        return timelines.get(timelines.size() - 1);
    }

    private List<String> getAllTimelinesText()
    {
        waitUntilTrue(timelineContainer.timed().isVisible());
        return timelineContainer.findAll(By.cssSelector(".roadmap-column-title"))
                .stream()
                .map(PageElement::getText)
                .collect(toList());
    }

    public void clickAddBar()
    {
        addBarButton.click();
    }

    public BarDialog clickOnBar(String barTitle)
    {
        PageElement barElement = findBar(barTitle);
        barElement.click();
        return pageBinder.bind(BarDialog.class);
    }

    public List<String> getAllLanesTitle()
    {
        return roadmapEditorDialog.findAll(By.cssSelector(".roadmap-lane-title"))
                .stream()
                .map(item -> item.find(By.cssSelector(".title-inner")).getAttribute("innerHTML")) //trick to get text which has been rotated
                .collect(toList());
    }

    public String getLaneColor(String laneTitle)
    {
        List<PageElement> laneElements = roadmapEditorDialog.findAll(By.cssSelector(".roadmap-lane-title"));
        for (PageElement laneElement : laneElements)
        {
            String title = laneElement.find(By.cssSelector(".title-inner")).getAttribute("innerHTML"); //trick to get text which has been rotated
            if(StringUtils.equals(title, laneTitle)) {
                Map<String, String> styleParameters =
                        RoadmapStringUtils.getParameters(laneElement.getAttribute("style"), ";", ":");
                return styleParameters.get("background-color");
            }
        }
        return "";
    }

    public RoadmapEditorDialog scrollMarkerIntoView(String markerTitle){
        PageElement markerElement = findMarker(markerTitle);
        if(markerElement != null){
            markerElement.javascript().execute("arguments[0].scrollIntoView()");
        }
        return this;
    }

    public MarkerDialog clickOnMarker(String markerTitle)
    {
        PageElement markerElement = findMarker(markerTitle);
        if (markerElement == null)
        {
            return null;
        }
        markerElement.click();
        return pageBinder.bind(MarkerDialog.class);
    }

    public List<String> getAllBarTitles()
    {
        return roadmapEditorDialog.findAll(By.cssSelector(".roadmap-bar"))
                .stream()
                .map(PageElement::getText)
                .collect(toList());
    }

    public boolean containMarker(String markerTitle)
    {
        return findMarker(markerTitle) != null;
    }

    private PageElement findMarker(String markerTitle)
    {
        return roadmapEditorDialog.findAll(By.cssSelector(".marker-title"))
                .stream()
                .filter(item -> Objects.equals(markerTitle, item.getText()))
                .findFirst()
                .orElse(null);
    }

    private PageElement findBar(String barTitle)
    {
        return roadmapEditorDialog.findAll(By.className("roadmap-bar-title"))
                .stream()
                .filter(item -> Objects.equals(barTitle, item.getText()))
                .findFirst()
                .orElse(null);
    }

    public String getStartDate()
    {
        return startDate.getValue();
    }

    public String getEndDate()
    {
        return endDate.getValue();
    }

    public void setStartDate(String date)
    {
        setDate(startDate, date);
    }

    public void setEndDate(String date)
    {
        setDate(endDate, date);
    }

    private void setDate(PageElement dateElement, String date)
    {
        waitUntilTrue(dateElement.timed().isVisible());
        dateElement.clear().type(date);
        dateElement.click();
        String dpPopupUuid = dateElement.getAttribute("data-aui-dp-uuid");

        AUIDatePickerDialog auiDatePicker = pageBinder.bind(AUIDatePickerDialog.class, dpPopupUuid);
        auiDatePicker.chooseSelectedDay();
        dateElement.javascript().form().change();
    }

    public void typeStartDate(String date)
    {
        startDate.clear().type(date);
        startDate.javascript().form().change();
    }

    public void typeEndDate(String date)
    {
        endDate.clear().type(date);
        endDate.javascript().form().change();
    }

    public boolean hasErrorMessage()
    {
        PageElement errorMessage = roadmapEditorDialog.find(By.id("roadmap-timeline-error"));
        return isNotEmpty(errorMessage.getText());
    }
}
